#include <stdio.h>
// #include <iostream> // C++
#include <stdlib.h>
#include <pthread.h>
#include <curl/curl.h>
#include <time.h>


void request(long thread_number, char* ip) {
    CURL *curl;
    CURLcode res;

    // if(curl) {
        curl = curl_easy_init();

        curl_easy_setopt(curl, CURLOPT_URL, ip);
        //curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "t%li: %s: %s\n", thread_number, ip, curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);
    // }
}



void* kernel(void* ptr) {

    long* number = ptr;

    long* before_request_number = &number[0];
    long* after_request_number = &number[1];
    char* ip = ((char*) number[2]); // pointer to IP char*

    (*before_request_number)++; // incrementing thread spawn number

    struct timeval t0;
    struct timeval t1;

    gettimeofday(&t0, 0);
    request(*before_request_number, ip); // in TRY maybe?
    gettimeofday(&t1, 0);

    (*after_request_number)++; // incrementing done request number

    printf("t%li: %fms\n",
        *after_request_number,
        (t1.tv_sec - t0.tv_sec)*1000.0f + (t1.tv_usec - t0.tv_usec)/1000.0f);

    return NULL;
}



int main(int argc, char *argv[]) {

    char* ip = "192.168.56.101:8081"; // IP string, can be replaced by argv[1] to avoid ip hardcoding

    if (argc > 1) {
        for (int count = 1; count < argc; count++) {
            printf("argv[%d] = %s\n", count, argv[count]);
        }
    } else {
        printf("The command had no other arguments.\n");
    }

    struct timespec tim, tim2;
    tim.tv_sec = 0; tim.tv_nsec = 500;

    int thread_number = 1000, i = 0, ret = -1;
    long number[] = {0, 0, (long) ip}; // {thread spawn number, done request number, pointer to ip variable to pass}
    double total_time_seconds = thread_number*tim.tv_nsec/1000000000.0;

    // int histogram[1000*60*10] // 1000ms in second, 60s in minute, 10 minutes
    // pass it to the request((int*) histogram), then increment histogram[(int) delay]++

    pthread_t* threads = (pthread_t*) malloc(sizeof(pthread_t[thread_number]));
    printf("total spawning time: %fs\n", total_time_seconds);

    for (i = 0; i < thread_number; i++) {

        if(nanosleep(&tim , &tim2) < 0 ) {
            fprintf(stderr, "Nano sleep system call failed \n"); return -1;
        }

        ret = pthread_create(&threads[i], NULL, kernel, &number);

        if(ret != 0) {
            fprintf(stderr, "Create pthread error!\n"); exit (1);
        }
    }

    printf("done creating threads");

    getchar();

    free(threads);

    printf("spawned %li, requested %li \n", number[0], number[1]);

    // for (int i = 0; i < 1000; i++) {
    //     request();
    // }


    return 0;
}
