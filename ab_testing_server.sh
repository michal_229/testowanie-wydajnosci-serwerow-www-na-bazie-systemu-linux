#!/bin/bash

work_dir=$(pwd)
server_ip="192.168.56.101"
workspace_dir="$work_dir"/"temp-work-files"
vm_folder="/media/michal/WD-EXT4/maszyny wirtualne/"
vm_name="ubuntu1404server64"

serwery=( "apache" "lighttpd" "nginx" "nodejs")

wersje_maszyn=( "1024MB-100CPU" "1024MB-010CPU" "0128MB-100CPU" "0128MB-010CPU" )

pliki=( "file_10_kb" "file_100_kb" "file_1024_kb" )

iter_number=4


function pause {
   read -p "$*"
}

function iteracja_testu {

    server_name=$1
    nr_maszyny=$2
    plik=$3
    numer_iteracji=$4

    id_testu="$server_name"_"$nr_maszyny"_"$plik"_"$numer_iteracji"
    wynik_testu="test"_"$id_testu"

    echo ">>> "$id_testu

    #-n = Number of requests
    #-c = simult. connections
    #-g = output file
    # 1st core
    taskset 1 ab -n 1000 -c 10 -g "$wynik_testu" "$server_ip"/"$plik" > "log_ab"_"$id_testu"
}

function set_virtual_machine_specs {
    #!/bin/bash

    ver=$1

    case $ver in
        1024MB-100CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap 100 --memory 1024
            ;;
        1024MB-010CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap  10 --memory 1024
            ;;
        1024MB-001CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap   1 --memory 1024
            ;;

        0128MB-100CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap 100 --memory  128
            ;;
        0128MB-010CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap  10 --memory  128
            ;;
        0128MB-001CPU)
            VBoxManage modifyvm "$vm_name" --cpuexecutioncap   1 --memory  128
            ;;

        *)
            echo "!!! zla konfiguracja !!!"
            ;;
    esac

}

mkdir "$workspace_dir"

for server in "${serwery[@]}"
do
    echo -e "Executing bench: $server_name $server_ip ..."

    cd "$workspace_dir"
    mkdir $server
    cd "$workspace_dir/$server"

    cd "$vm_folder"
    VBoxManage snapshot "$vm_name" restore "$server"-startpoint
    current_timestamp=`date +%Y-%m-%d-%H-%M-%S`

    for maszyna in "${wersje_maszyn[@]}"
    do

        cd "$vm_folder"
        snapshot_name="$server"-startpoint
        set_virtual_machine_specs $maszyna

        # 2nd core
        taskset 2 VBoxManage startvm "$vm_name" --type headless

        echo "wlaczanie $server on VM $vm_name $snapshot_name $ver"

        while ping -c 1 $server_ip | grep "100% packet loss" ; do echo "not pinged back" && sleep 1;  done
        echo "ping received, waiting 10s"
        sleep 10

        cd $workspace_dir/$server

        for plik in "${pliki[@]}"
        do
            sleep 10s
            for iter in `seq 1 $iter_number`
            do
                sleep 5s
                iteracja_testu $server $maszyna $plik $iter
            done

        done

        cd "$vm_folder"
        echo "wylaczanie $server on VM $vm_name $snapshot_name $ver"
        VBoxManage controlvm "$vm_name" poweroff
    done

    VBoxManage snapshot "$vm_name" take "after testing $current_timestamp"
done
