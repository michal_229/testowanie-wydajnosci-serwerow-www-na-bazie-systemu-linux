\contentsline {section}{\numberline {1}Zadanie}{3}
\contentsline {section}{\numberline {2}Pomiary czasu przetwarzania \IeC {\.z}\IeC {\k a}dania}{4}
\contentsline {subsection}{\numberline {2.1}Pomiary dla maszyny wirtualnej 128MB RAM}{4}
\contentsline {subsubsection}{\numberline {2.1.1}Pomiary dla maszyny wirtualnej 10\% CPU}{4}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 10kB od czas jego przetworzenia:}{4}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 10kB:}{5}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 100kB od czas jego przetworzenia:}{6}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 100kB:}{7}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 1024kB od czas jego przetworzenia:}{8}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 1024kB:}{9}
\contentsline {subsubsection}{\numberline {2.1.2}Pomiary dla maszyny wirtualnej 100\% CPU}{10}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 10kB od czas jego przetworzenia:}{10}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 10kB:}{11}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 100kB od czas jego przetworzenia:}{12}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 100kB:}{13}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 1024kB od czas jego przetworzenia:}{14}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 1024kB:}{15}
\contentsline {subsection}{\numberline {2.2}Pomiary dla maszyny wirtualnej 1024MB RAM}{16}
\contentsline {subsubsection}{\numberline {2.2.1}Pomiary dla maszyny wirtualnej 10\% CPU}{16}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 10kB od czas jego przetworzenia:}{16}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 10kB:}{17}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 100kB od czas jego przetworzenia:}{18}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 100kB:}{19}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 1024kB od czas jego przetworzenia:}{20}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 1024kB:}{21}
\contentsline {subsubsection}{\numberline {2.2.2}Pomiary dla maszyny wirtualnej 100\% CPU}{22}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 10kB od czas jego przetworzenia:}{22}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 10kB:}{23}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 100kB od czas jego przetworzenia:}{24}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 100kB:}{25}
\contentsline {paragraph}{Wykres numeru \IeC {\.z}\IeC {\k a}dania 1024kB od czas jego przetworzenia:}{26}
\contentsline {paragraph}{Histogram czasu przetwarzania \IeC {\.z}\IeC {\k a}dania 1024kB:}{27}
\contentsline {section}{\numberline {3}Pomiary zaj\IeC {\k e}to\IeC {\'s}ci zasob\IeC {\'o}w maszyny wirtualnej}{28}
\contentsline {subsection}{\numberline {3.1}podsekcja}{28}
\contentsline {paragraph}{paragraf}{28}
\contentsline {section}{\numberline {4}Wnioski}{29}
