#!/bin/bash

# connect to server vm
# usage:
# ./connect_server_ssh.sh             # default IP
# ./connect_server_ssh.sh <ip.here>   # custom IP


if [ -z "$1" ];
then
    ip="192.168.56.101"
else
    ip="$1"
fi

user="test16"

while [ true ]
do
    ssh -X "$user"@"$ip"
    sleep 5s
done
