#!/bin/bash

wersje_maszyn=( "1024MB-100CPU" "1024MB-010CPU" "0128MB-100CPU" "0128MB-010CPU" )
nazwy_serwerow=( "apache" "lighttpd" "nginx" "nodejs" )
xrange=( 40 260 100 350 )

katalog_pomiarow="bez_zer"
wd=$(pwd)



lista_serwerow=(./$katalog_pomiarow/*-vm)


for s in "${lista_serwerow[@]}"
do
    pomiary=(./$s/*_test_*)
    for (( i=0; i<${#pomiary[@]}; i++ ))
    do
        konfiguracja_wykresu="./RAM/gnuplot_config"_${wersje_maszyn[$i]}
        wykres_wynikowy="./RAM/wykres"_${wersje_maszyn[$i]}.png

        echo "set terminal png truecolor giant size 1000, 1000 "   > $konfiguracja_wykresu
        echo "set output \"$wykres_wynikowy\""                    >> $konfiguracja_wykresu
        echo "set title \"MEMORY\""                               >> $konfiguracja_wykresu
        echo "set size 1.0,0.25"                                  >> $konfiguracja_wykresu
        echo "set grid x,y"                                       >> $konfiguracja_wykresu
        echo "set boxwidth 1.0"                                   >> $konfiguracja_wykresu
        echo "set style fill transparent solid 0.5 noborder"      >> $konfiguracja_wykresu
        echo "set style line 1 lc rgb \"red\""                    >> $konfiguracja_wykresu
        echo "set style line 2 lc rgb \"blue\""                   >> $konfiguracja_wykresu
        echo "set style line 3 lc rgb \"orange\""                 >> $konfiguracja_wykresu
        echo "set style line 4 lc rgb \"green\""                  >> $konfiguracja_wykresu
        echo "set xrange [0:${xrange[$i]}]"                       >> $konfiguracja_wykresu
        echo "set yrange [0:300]"                                 >> $konfiguracja_wykresu
        echo "set xlabel \"time [s]\""                            >> $konfiguracja_wykresu
        echo "set ylabel \"MEMORY [MB]\""                         >> $konfiguracja_wykresu
        echo "set multiplot"                                      >> $konfiguracja_wykresu
        # printf "plot " >> $konfiguracja_wykresu
    done
done


for (( j=0; j<${#lista_serwerow[@]}; j++ ))
do
    s=${lista_serwerow[$j]}
    pomiary=(./$s/*_test_*)

    # for f in "${pomiary[@]}"
    for (( i=0; i<${#pomiary[@]}; i++ ))
    do

        konfiguracja_wykresu="./RAM/gnuplot_config"_${wersje_maszyn[$i]}


        f=${pomiary[$i]}
        echo "set origin 0.0,0.25*$j" >> $konfiguracja_wykresu
        echo "plot \"$f\" using 2 with boxes ls $(( $j+1 )) title \"mem ${nazwy_serwerow[$j]} ${wersje_maszyn[$i]}\", " >> $konfiguracja_wykresu
    done
done
# every ::2

for s in "${lista_serwerow[@]}"
do
    pomiary=(./$s/*_test_*)
    for (( i=0; i<${#pomiary[@]}; i++ ))
    do
        echo "unset multiplot  ">> $konfiguracja_wykresu
        konfiguracja_wykresu="./RAM/gnuplot_config"_${wersje_maszyn[$i]}
        gnuplot                    $konfiguracja_wykresu
    done
done

rm ./RAM/gnuplot_config*
