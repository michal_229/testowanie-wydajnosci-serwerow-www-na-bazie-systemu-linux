#!/bin/bash

server="lighttpd"
log_file="$server""_test_log_`date +%Y-%m-%d-%H-%M-%S`"
echo "logging to $log_file"
dstat -cmdngyr >> $log_file
