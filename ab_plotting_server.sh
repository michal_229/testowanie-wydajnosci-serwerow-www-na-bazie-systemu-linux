#!/bin/bash

work_dir=$(pwd)
workspace_dir="$work_dir"/"temp-work-files"

serwery=( "apache" "lighttpd" "nginx" "nodejs")

wersje_maszyn=( "1024MB-100CPU" "1024MB-010CPU" "0128MB-100CPU" "0128MB-010CPU" )

pliki=( "file_10_kb" "file_100_kb" "file_1024_kb" )

iter_number=4


# for 4 servers, 4 vm versions
function make_graphs_for_servers {
    server_name="--"
    nr_maszyny="--"
    plik="--"
    numer_iteracji="--"

    # wykres czasu obslugi od nr requestu
    for m in "${wersje_maszyn[@]}"
    do
        for p in "${pliki[@]}"
        do
            id_testu="$server_name"_"$m"_"$p"_"$numer_iteracji"_time-num
            konfiguracja_wykresu="gnuplot"_"$id_testu"
            wykres_wynikowy="wykres"_"$id_testu".png

            echo ">>> graphs for "$id_testu

            echo "set terminal png giant size 1024,1024 " > $konfiguracja_wykresu
            echo "set output \"$wykres_wynikowy\"" >> $konfiguracja_wykresu
            echo "set title \"Zaleznosc czasu obslugi zadania do jego numeru\"" >> $konfiguracja_wykresu
            echo "set size 1,1" >> $konfiguracja_wykresu
            echo "set grid x,y" >> $konfiguracja_wykresu
            echo "set logscale y 10" >> $konfiguracja_wykresu
            echo "set xlabel \"Request\"" >> $konfiguracja_wykresu
            echo "set ylabel \"Response time (ms)\"" >> $konfiguracja_wykresu
            printf "plot " >> $konfiguracja_wykresu

            for s in "${serwery[@]}"
            do
                # for i in `seq 2 $iter_number`
                # do

                i=3

                id="$s"_"$m"_"$p"_"$i"
                wyn="test"_"$id"
                printf "\"./$s/$wyn\" using 9 smooth sbezier with lines title \"$s $m $p $i\", " >> $konfiguracja_wykresu
                # done
            done

            gnuplot $konfiguracja_wykresu
        done
    done

    # wykres histogramu czasu obslugi
    for m in "${wersje_maszyn[@]}"
    do
        for p in "${pliki[@]}"
        do
            id_testu="$server_name"_"$m"_"$p"_"$numer_iteracji"_hist
            konfiguracja_wykresu="gnuplot"_"$id_testu"
            wykres_wynikowy="wykres"_"$id_testu".png

            echo ">>> graphs for "$id_testu

            echo "set terminal png giant size 1024,1024 " > $konfiguracja_wykresu
            echo "set output \"$wykres_wynikowy\"" >> $konfiguracja_wykresu
            echo "set title \"Histogram czasu obslugi zadania\"" >> $konfiguracja_wykresu
            echo "set size 1,1" >> $konfiguracja_wykresu
            echo "set grid y" >> $konfiguracja_wykresu
            echo "set xlabel \"Time \"" >> $konfiguracja_wykresu
            echo "set ylabel \"Number\"" >> $konfiguracja_wykresu
            echo "set style histogram clustered" >> $konfiguracja_wykresu
            echo "set style fill transparent solid 1.0 noborder"  >> $konfiguracja_wykresu
            echo "set style line 1 lt 8 lw 9 pt 3 ps 0.5" >> $konfiguracja_wykresu
            echo "set xrange [10:*]" >> $konfiguracja_wykresu
            echo "set yrange [0:*]" >> $konfiguracja_wykresu
            echo "set logscale x 10" >> $konfiguracja_wykresu
            echo "set boxwidth 10" >> $konfiguracja_wykresu
            echo "bin(x) = floor(x/10.0)*10.0 + 5.0 " >> $konfiguracja_wykresu
            printf "plot " >> $konfiguracja_wykresu

            for s in "${serwery[@]}"
            do
                # for i in `seq 2 $iter_number`
                # do

                i=3

                id="$s"_"$m"_"$p"_"$i"
                wyn="test"_"$id"
                printf "\"./$s/$wyn\" using (bin(\$9)):(1.0) smooth freq w lines title \"$s $m $p $i\", " >> $konfiguracja_wykresu
                # done
            done

            gnuplot $konfiguracja_wykresu
        done
    done
}

cd $workspace_dir

make_graphs_for_servers
